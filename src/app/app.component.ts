import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  users: Array<any>;

  // Create an instance of the DataService through dependency injection
  constructor(private _router: Router, private _authService: AuthService) { }

  ngInit() {
    if ( this._authService.isAuthenticated() ) {
      this._authService.signOut();
      this._router.navigate(['/']);
    }
  }

  // User Logout
  /*
    IR: Instead of authentication status property change
    just call the Sign Out function to remove the token from Local Storage
    and redirect to the home page
  */
  signOut() {
    this._authService.signOut();
  }


}

import { Component, OnInit } from '@angular/core';

// Services
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit {

  constructor(public _authService: AuthService ) { }

  ngOnInit() {
  }

  public checkToken() {
    console.log('IS AUTH:' + this._authService.isAuthenticated());
  }

}
